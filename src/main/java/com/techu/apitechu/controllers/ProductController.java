package com.techu.apitechu.controllers;
import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String API_BASE_URL = "/apitechu/v1";

    @GetMapping(API_BASE_URL + "/products")
    public ArrayList<ProductModel> getProducts()
    {
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }
    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id)
    {
        System.out.println("getProductById");
        System.out.println("id es " + id);
        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
                 if (product.getId().equals(id)){
                     result=product;
                 }
        }
        return result;

    }
    @PostMapping(API_BASE_URL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es" + newProduct.getId());
        System.out.println("La descripcion del nuevo producto es" + newProduct.getDesc());
        System.out.println("La precio del nuevo producto es" + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }
    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product,@PathVariable String id) {
        System.out.println("updateProduct");

        System.out.println("La id del producto a actualizar en parametro URL" + id);
        System.out.println("La id del producto a actualizar es" + product.getId()) ;
        System.out.println("La descripcion del nuevo actualizar" + product.getDesc());
        System.out.println("La precio del nuevo pactualizares" + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }


    return  product;

    }
    @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");
        System.out.println("objeto recibido"+ productData.getId());
        System.out.println("Descripcion "+ productData.getDesc());

        System.out.println("Precio"+ productData.getPrice());

        ProductModel result = new ProductModel();
        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("encontrado");
                result= productInList;

                if (productData.getDesc() != null) {
                    System.out.println("actualizando descripicion");
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() > 0 ) {
                    System.out.println("actualizando precio");
                    productInList.setPrice(productData.getPrice());
                }

            }
        }

        return result;

    }

    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es  " + id);
     //   System.out.println("La id del producto a a borrar" + product.getId()) ;
     //   System.out.println("La descripcion del nuevo borrar" + product.getDesc());
     //   System.out.println("La precio del nuevo borrar" + product.getPrice());

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("producto a borrar encontrado" );
                foundProduct = true;
                result = productInList;
                 }
        }
        if (foundProduct == true) {
            System.out.println("borrando producto" );
            ApitechuApplication.productModels.remove(result);
        }
        return result;
    }
}
